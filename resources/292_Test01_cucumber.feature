# Automation priority: null
# Test case importance: Low
# language: en
Feature: Test01 cucumber

	Scenario Outline: Test01 cucumber
		Given I am logged in
		When I am on the Home page
		And I navigate to the category <CATEGORY>
		And I navigate to product <PRODUCT>
		And I add to cart
		Then The cart contains
			| Product                                  | Number | Dimension | Size | Color |
			| Affiche encadrÃ©e The best is yet to come |      1 | 40x60cm   |      |       |

		@bdd_dataset
		Examples:
		| CATEGORY | PRODUCT |
		| "art" | "Affiche EncadrÃ©e The Best..." |