package prestashoptest;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import prestashoptest.seleniumtools.PageObjectBase;

public class SetupTeardown {

    /**
     * test setup
     */
    @Before
    public void setup() {
        PageObjectBase.setBrowser(PageObjectBase.Browser.FIREFOX);
        PageObjectBase.setHost("http://localhost:8080/fr");
    }

    /**
     *
     * test teardown (take a screenshot if the test failed, then close the browser)
     *
     * @param scenario
     */
    @After
    public void closeBrowser(final Scenario scenario) {
        if (scenario.isFailed()) {
            final byte[] screenshot = PageObjectBase.getScreenshot();
            final String screenshotName = "screenshot " + scenario.getName() + " (line " + scenario.getLine() + ")";
            scenario.attach(screenshot,"image/png", screenshotName);
        }
        PageObjectBase.quit();
    }
}
